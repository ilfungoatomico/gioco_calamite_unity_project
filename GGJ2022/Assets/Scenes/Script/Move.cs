using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{

    [SerializeField] int velocityMultiplier;
    public bool canO;
    public bool canV;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (canO)
            gameObject.transform.position += new Vector3(Input.GetAxis("Horizontal") * velocityMultiplier * Time.fixedDeltaTime, 0, 0);
        //gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(Input.GetAxis("Horizontal") * velocityMultiplier * Time.fixedDeltaTime , 0));
        if (canV)
        {
            gameObject.transform.position += new Vector3(0, Input.GetAxis("Vertical") * velocityMultiplier * Time.fixedDeltaTime, 0);
            gameObject.GetComponent<Rigidbody2D>().gravityScale = 0.01f;
        }
        else
        {
            gameObject.GetComponent<Rigidbody2D>().gravityScale = 1f;
        }
           
        //gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2( 0, Input.GetAxis("Vertical") * velocityMultiplier * Time.fixedDeltaTime));
    }
}
