using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour
{
    // Start is called before the first frame update
    //[SerializeField] string tag1;
    //[SerializeField] string tag2;
    private void OnTriggerEnter2D(Collider2D collision)
    {
       
        if ( collision.gameObject.tag == "Player")
        {
            collision.GetComponentInParent<Flip>().Flip1();
        }
    }
}
