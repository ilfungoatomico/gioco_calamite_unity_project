using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckMove : MonoBehaviour
{
    // Start is called before the first frame update
    Move move;
    [SerializeField] string myObject;
    private void Start()
    {
        move= gameObject.GetComponentInParent<Move>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (myObject == "ground")
        {
            if (collision.tag == "ground")
            {
                move.canO = true;
            }
        }
        if (myObject == "pillar")
        {
            if (collision.tag == "pillar")
            {
                move.canV = true;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {

        if (myObject == "ground")
        {
            if (collision.tag == "ground")
            {
                move.canO = true;
            }
        }
        if (myObject == "pillar")
        {
            if (collision.tag == "pillar")
            {
                move.canV = true;
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (myObject == "ground")
        {
            if (collision.tag == "ground")
            {
                move.canO = false;
            }
        }
        if(myObject == "pillar")
        {
            if (collision.tag == "pillar")
            {
                move.canV = false;
            }
        }
       
    }
}
