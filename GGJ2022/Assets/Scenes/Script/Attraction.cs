using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attraction : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] int forceFactor;
    private Vector2 _destiantion = new Vector2(0,0);
    public Rigidbody2D objectToAttract;
    public bool isAttracted = false;
    public bool isRepulsed = false;
   public GameObject parent;
    public void SetDestination(Vector2 destination)
    {
        _destiantion = destination;
    }

    private void FixedUpdate()
    {
       
        if (objectToAttract != null)
        {
            if(isAttracted)
            {
                //Debug.Log("gb" + parent.name + " " + parent.transform.position + "ot" + objectToAttract.transform.position.x);
                objectToAttract.AddForce((parent.transform.position - objectToAttract.transform.position) * forceFactor * Time.fixedDeltaTime);
                parent.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0; ;
            }
           
            if (isRepulsed)
            {
                //Debug.Log("gb" + parent.name + " " + parent.transform.position + "ot" + objectToAttract.transform.position.x);
                objectToAttract.AddForce((parent.transform.position - objectToAttract.transform.position) * -forceFactor * Time.fixedDeltaTime);
           
            }
        }
    }
}
