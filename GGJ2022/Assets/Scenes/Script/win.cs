using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class win : MonoBehaviour
{
    [SerializeField] string tag1;
    [SerializeField] string tag2;
    [SerializeField] Text win1;
    [SerializeField] float timeToWait;
    [SerializeField] ChangeScene sceneLoader;
    [SerializeField] GameObject vfx;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Win " + collision.gameObject.name );
        if(collision.gameObject.tag == tag1 || collision.gameObject.tag == tag2 || collision.gameObject.tag == "Player")
        {
            win1.text = "WIN";
            if(vfx != null)
                Instantiate(vfx, collision.transform.position, Quaternion.identity);
            StartCoroutine(Wait());
            
        }
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(timeToWait);
        sceneLoader.Loader();
    }
}
