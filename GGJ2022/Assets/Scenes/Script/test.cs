using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour
{// Start is called before the first frame update
 // Start is called before the first frame update
    [SerializeField] string ptag;
    [SerializeField] string ntag;
    [SerializeField] Attraction attraction;
    [SerializeField] Transform targetTransform;
    [SerializeField] GameObject sNegative;
    [SerializeField] GameObject sPostive;
    [SerializeField] int nDistance;
    [SerializeField] int pDistance;

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("trigger " + other.tag);
        if (other.tag == ptag)
        {
            
        }
        if (other.tag == ntag)
        {
            
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == ptag)
        {
            targetTransform = null;
            attraction.isAttracted = false;
            attraction.objectToAttract = null;
        }
        if (other.tag == ntag)
        {
            targetTransform = null;
            attraction.isRepulsed = false;
            attraction.objectToAttract = null;
        }

    }
}
