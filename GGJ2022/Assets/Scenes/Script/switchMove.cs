using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switchMove : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Move move1;
    [SerializeField] GameObject aura1;
    [SerializeField] Move move2;
    [SerializeField] GameObject aura2;
    bool switchM = true;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            aura1.GetComponent<SpriteRenderer>().enabled = !switchM;
            move1.enabled = !switchM;
            aura2.GetComponent<SpriteRenderer>().enabled = switchM;
            move2.enabled = switchM;
            switchM = !switchM;
        }
    }
}
