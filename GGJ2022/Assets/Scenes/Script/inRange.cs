using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inRange : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] string targetTag;
    [SerializeField] string targetTagInverted;
    [SerializeField] Attraction attraction;
    [SerializeField] Transform targetTransform;

    private void OnTriggerEnter2D(Collider2D other)
    {
       // Debug.Log("trigger " + other.tag );
        if (other.tag == targetTag && !attraction.isRepulsed)
        {
           // Debug.Log("ciao");
            targetTransform = other.gameObject.GetComponentInParent<Transform>();
            attraction.SetDestination(new Vector2(targetTransform.position.x, targetTransform.position.y));
            attraction.isAttracted = true;
            attraction.objectToAttract = other.gameObject.GetComponentInParent<Rigidbody2D>();
        }
        if (other.tag == targetTagInverted && !attraction.isAttracted)
        {
            //Debug.Log("ciao");
            targetTransform = other.gameObject.GetComponentInParent<Transform>();
            attraction.SetDestination(new Vector2(targetTransform.position.x, targetTransform.position.y));
            attraction.isRepulsed = true;
            attraction.objectToAttract = other.gameObject.GetComponentInParent<Rigidbody2D>();
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        //if (other.tag == targetTag)
        //{
        //    targetTransform = null;
        //    attraction.isAttracted = false;
        //    attraction.objectToAttract = null;
        //}
        if (other.tag == targetTagInverted)
        {
            targetTransform = null;
            attraction.isRepulsed = false;
            attraction.objectToAttract = null;
        }

    }
}
