using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flip : MonoBehaviour
{

    [SerializeField] KeyCode keyFlip;
    [SerializeField] bool flipped = true;
    [SerializeField] GameObject other;
    public GameObject faceToChange;
    [SerializeField] Sprite faceUp;
    [SerializeField] Sprite faceDown;

    //[SerializeField] bool negative = true;
    //[SerializeField] bool positive = true;
    //[SerializeField] BoxCollider2D faceNegative;
    //[SerializeField] BoxCollider2D facePositive;

    private void Start()
    {
        if (gameObject.transform.rotation.z == 0)
            flipped = true;
        else
            flipped = false;
    }


    public void Flip1()
    {
        
            //negative = !negative;
            //positive = !positive;  
            //Debug.Log("n ");
            //Debug.Log("p " + positive);
           
            
            if(other.GetComponent<Flip>().flipped)
            {
                Debug.Log("180 ");
                other.transform.rotation = Quaternion.Euler(0, 0, 180);
                other.GetComponent<Flip>().faceToChange.GetComponent<SpriteRenderer>().sprite = other.GetComponent<Flip>().faceUp;
                other.GetComponent<Flip>().flipped = false;
            }
            else
            {
                Debug.Log("0 ");
                other.transform.rotation = Quaternion.Euler(0, 0, 0);
                other.GetComponent<Flip>().faceToChange.GetComponent<SpriteRenderer>().sprite = other.GetComponent<Flip>().faceDown;
                other.GetComponent<Flip>().flipped = true;
            }
                
        }
        //faceNegative.enabled = negative;
        //facePositive.enabled = positive;

    
}
