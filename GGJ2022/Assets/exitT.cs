using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class exitT : MonoBehaviour
{
    [SerializeField] Transform exitTTransform;
    [SerializeField] Attraction attraction1;
    [SerializeField] Attraction attraction2;
    //[SerializeField] Attraction attraction3;
    //[SerializeField] Attraction attraction4;

    [SerializeField] string targetTag;
    [SerializeField] string targetTagInverted;
    private void OnTriggerExit2D(Collider2D other)
    {
        
        
        if (other.tag == targetTag || other.tag == targetTagInverted || other.tag == "Player")
        {
           // Debug.Log("exit " + other.tag);
            exitTTransform = other.gameObject.GetComponentInParent<Transform>();
            exitTTransform = null;
            attraction1.isAttracted = false;
            attraction1.isRepulsed = false;
            attraction1.objectToAttract = null;
            attraction2.isRepulsed = false;
            attraction2.isAttracted = false;
            attraction2.objectToAttract = null;
            //attraction3.isRepulsed = false;
            //attraction3.isAttracted = false;
            //attraction3.objectToAttract = null;
            //attraction4.isRepulsed = false;
            //attraction4.isAttracted = false;
            //attraction4.objectToAttract = null;

            attraction1.parent.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
            attraction2.parent.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
            //attraction3.parent.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
            //attraction4.parent.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
        }

    }
}
